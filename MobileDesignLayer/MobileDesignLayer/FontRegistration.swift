//
//  FontRegistration.swift
//  MobileDesignLayer
//
//  Created by Tcs on 13/10/22.
//
import UIKit
import Foundation
enum RegisterFontError: Error {
  case invalidFontFile
  case fontPathNotFound
  case initFontError
  case registerFailed
}
class GetBundle {}
//extension UIFont {
//static func register(path: String, fileNameString: String, type: String) throws {
//  let frameworkBundle = Bundle(for: GetBundle.self)
//  guard let resourceBundleURL = frameworkBundle.path(forResource: path + "/" + fileNameString, ofType: type) else {
//     throw FontError.fontPathNotFound
//  }
//  guard let fontData = NSData(contentsOfFile: resourceBundleURL),    let dataProvider = CGDataProvider.init(data: fontData) else {
//    throw FontError.invalidFontFile
//  }
//  guard let fontRef = CGFont.init(dataProvider) else {
//     throw FontError.initFontError
//  }
//  var errorRef: Unmanaged<CFError>? = nil
// guard CTFontManagerRegisterGraphicsFont(fontRef, &errorRef) else   {
//       throw FontError.registerFailed
//  }
// }
//    //Either lazy or static is ok
//    static var registerFonts: () -> Void = {
//        UIFont.register(path: <#T##String#>, fileNameString: <#T##String#>, type: <#T##String#>)
//      UIFont.register(path:xxx, fileName: xxx, type: .ttf)
//
//    }()
//}
