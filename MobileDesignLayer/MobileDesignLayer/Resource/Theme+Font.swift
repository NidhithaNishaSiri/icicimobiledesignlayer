//
// Created by Tcs on 10/10/22.
//

import UIKit

public extension Theme {
    var font: UIFont {
        switch self {
            case .fontsAccess: return Theme.iMobileMulishBoldFont15()
            default: return Theme.iMobileMulishRegularFont15()
        }
    }

    static func iMobileMulishRegularFont15(ofSize fontSize: CGFloat = 15) -> UIFont {
        UIFont.register(font: "Mulish-Regular", type: "ttf")
        return UIFont(name: "Mulish-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }

    static func iMobileMulishBoldFont15(ofSize fontSize: CGFloat = 15) -> UIFont {
        UIFont.register(font: "Mulish-Bold", type: "ttf")
        return UIFont(name: "Mulish-Bold", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
}
