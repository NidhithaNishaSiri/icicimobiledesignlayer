//
//Created by Tcs on 10/10/22.
//

import Foundation
import UIKit

@objc(IMOBILETheme)
public enum Theme: Int {
    case iMobile
    case iMobileUK
    case fontsAccess
    case partnership
}
