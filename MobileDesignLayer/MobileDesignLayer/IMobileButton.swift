//
//  IMobileButton.swift
//  IMobile
//

import UIKit

public enum IMobileButtonStye {
    case primaryOrange
    case secondaryMaroon
    case tertiaryBlue
    case pastelBlue
    case pastelBrown
    case boarderPrimary
    case primarySelected
    case primaryDisable
}

public enum IMobileButtonImage {
    case arrow
    case plus
    case empty
}

public enum IMobileButtonImagePosition {
    case left
    case right
}

public class IMobileButton: UIButton {
    
    public var btnImage: UIImage?
    public var disabledButtonImage: UIImage?
    public var imagePosition: IMobileButtonImagePosition = .right
    public let imageMargin: CGFloat = 14
    public var style = IMobileButtonStye.primaryOrange
    
    public let cornerRadius: CGFloat = 16
    public let cornerRadiusTxtFld: CGFloat = 16
    
    public var _shadow = false
    var shadow: Bool {
        get {
            return _shadow
        }
        set {
            _shadow = newValue
            
            if _shadow {
                addShadow()
            }else{
                removeShadow()
            }
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public init(frame: CGRect, style: IMobileButtonStye) {
        super.init(frame: frame)
        self.style = style
    }
    
    public init(frame: CGRect, style: IMobileButtonStye, image: IMobileButtonImage, shadow: Bool) {
        super.init(frame: frame)
        self.style = style
        apply(style: .primaryOrange,image: .empty, shadow: true)
    }
    
    required public init(coder: NSCoder) {
        super.init(coder: coder)!
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        adjustImageInsets()
    }
    
    public override var isEnabled: Bool {
        didSet {
            updateViews()
        }
    }
    
    private func updateViews() {
        updateButtonStyle()
    }
    
    private func adjustImageInsets() {
        
        guard let image = btnImage else {
            return
        }
        
        switch imagePosition {
        case .right:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: frame.width - image.size.width - imageMargin, bottom: 0,  right: 0)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -image.size.width + 1 , bottom: 0, right: image.size.width * 2.1)
        case .left:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: imageMargin , bottom: 0, right: 0)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: center.x/2 , bottom: 0, right: 0) 
        }
    }
    public  func apply(style: IMobileButtonStye, image: IMobileButtonImage = .arrow, imagePosition: IMobileButtonImagePosition = .right, shadow: Bool = true) {
      
        
        self.imagePosition = imagePosition
        self.style = style

        applyButtonStyle()
        addImage(image: image)
        self.shadow = shadow
    }
    
    public func applyButtonStyle() {
        
        titleLabel?.font = Theme.iMobileMulishBoldFont15(ofSize: 20)
        titleLabel?.numberOfLines = 1
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.minimumScaleFactor = 0.9
        titleLabel?.lineBreakMode = .byClipping
        
        layer.cornerRadius = cornerRadius
        
        updateButtonStyle()
    }
    
    public func updateButtonStyle() {
        switch style {
            
        case .primaryOrange:
            backgroundColor = isEnabled ? .primaryOrange : .primaryOrange80
            setTitleColor(isEnabled ? UIColor.white : UIColor.primaryOrange, for: .normal)
            tintColor = UIColor.white
        case .secondaryMaroon:
            backgroundColor = isHidden ? .secondaryMaroon : .white
            setTitleColor(isEnabled ? UIColor.white : UIColor.primaryOrange, for: .normal)
            tintColor = isEnabled ? UIColor.primaryOrange : UIColor.primaryOrange
            layer.borderWidth = 1
       
        case .tertiaryBlue:
            backgroundColor = isEnabled ? .tertiaryBlue : .tertiaryBlue80
            setTitleColor(isEnabled ? UIColor.white : UIColor.primaryOrange, for: .normal)
            tintColor = UIColor.white
            
        case .pastelBlue:
            backgroundColor = isEnabled ? .pastelBlue120 : .tertiaryBlue80
            setTitleColor(isEnabled ? UIColor.white : UIColor.primaryOrange, for: .normal)
            tintColor = UIColor.white

        case .pastelBrown:
            backgroundColor = isEnabled ? .pastelBrown110 : .pastelBrown90
            setTitleColor(isEnabled ? UIColor.white : UIColor.primaryOrange, for: .normal)
            tintColor = UIColor.white
            
        case .boarderPrimary:
            backgroundColor = isHidden ? .white : .white
            setTitleColor(isEnabled ? UIColor.black : UIColor.primaryOrange, for: .normal)
            setTitleColor(.primaryOrange, for: .normal)
            tintColor = UIColor.primaryOrange
            layer.borderWidth = 1
            
        case .primarySelected:
            backgroundColor = isEnabled ? .primaryOrange120 : .primaryOrange120
            setTitleColor(isEnabled ? UIColor.white : UIColor.primaryOrange, for: .normal)
            tintColor = UIColor.white
            
        case .primaryDisable:
            backgroundColor = isEnabled ? .bgGray100 : .bgGray100
            setTitleColor(isEnabled ? UIColor.white : UIColor.primaryOrange, for: .normal)
            tintColor = UIColor.gray
        }
        
        
        
        layer.borderColor = isEnabled ? UIColor.primaryOrange.cgColor : UIColor.primaryOrange.cgColor
    }
    
    func addImage(image: IMobileButtonImage) {
        
        switch image {
            case .arrow:
                btnImage = getArrowImage()
                disabledButtonImage = UIImage(named: "buttonArrow")!
            case .plus:
                btnImage = UIImage(named: "plus")!
                disabledButtonImage = UIImage(named: "plus")!
            case .empty:
                btnImage = nil
                disabledButtonImage = nil
        }
        
        if imagePosition == .left {
            contentHorizontalAlignment = .left
        }
        
        setImage(btnImage, for: .normal)
        setImage(btnImage, for: .highlighted)
        setImage(disabledButtonImage, for: .disabled)
    }
    
    public func getArrowImage() -> UIImage {
        
        var img = style == .primaryOrange ? UIImage(named: "buttonArrowBlue")! : UIImage(named: "buttonArrow")!
        
        if imagePosition == .left {
            img = UIImage(cgImage: img.cgImage!, scale: img.scale, orientation: .upMirrored)
        }
        
        return img
    }

    public func addShadow() {
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 0.7
    }
    
    public func removeShadow() {
        
        layer.shadowOpacity = 0
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 0
    }
    
    public func applyBorder() {
        
        layer.borderWidth = 1
    }
    
}
