//
//  IMobileColor.swift
//  MobileSDK
//
//  Created by Tcs on 10/10/22.
//

import Foundation
import UIKit
public extension UIColor {
    
    static let primaryOrange: UIColor  = UIColor(hex: "#F0792E")
    static let primaryOrange80: UIColor  = UIColor(hex: "#F7B68D")
    static let primaryOrange90: UIColor  = UIColor(hex: "#F3975D")
    static let primaryOrange100: UIColor  = UIColor(hex: "#F0792E")
    static let primaryOrange110: UIColor  = UIColor(hex: "#DB5E10")
    static let primaryOrange120: UIColor  = UIColor(hex: "#AB4A0C")
    
    static let secondaryMaroon: UIColor  = UIColor(hex: "#982F35")
    static let secondaryMaroon80: UIColor  = UIColor(hex: "#CE5F66")
    static let secondaryMaroon90: UIColor  = UIColor(hex: "#BF3B43")
    static let secondaryMaroon100: UIColor  = UIColor(hex: "#982F35")
    static let secondaryMaroon110: UIColor  = UIColor(hex: "#712327")
    static let secondaryMaroon120: UIColor  = UIColor(hex: "#4A171A")
    
    static let tertiaryBlue: UIColor  = UIColor(hex: "#20446C")
    static let tertiaryBlue80: UIColor  = UIColor(hex: "#3774B9")
    static let tertiaryBlue90: UIColor  = UIColor(hex: "#3774B9")
    static let tertiaryBlue100: UIColor  = UIColor(hex: "#20446C")
    static let tertiaryBlue110: UIColor  = UIColor(hex: "#142A43")
    static let tertiaryBlue120: UIColor  = UIColor(hex: "#091420")
    
    static let pastelBlue80: UIColor  = UIColor(hex: "#F7FAFC")
    static let pastelBlue90: UIColor  = UIColor(hex: "#EBF1F8")
    static let pastelBlue100: UIColor  = UIColor(hex: "#E3EDF8")
    static let pastelBlue110: UIColor  = UIColor(hex: "#99ADC2")
    static let pastelBlue120: UIColor  = UIColor(hex: "#99ADC2")

    static let pastelBrown90: UIColor  = UIColor(hex: "#F6F5F0")
    static let pastelBrown100: UIColor  = UIColor(hex: "#E3E0D0")
    static let pastelBrown110: UIColor  = UIColor(hex: "#CFCAAF")



    static let indicativeSuccess90: UIColor  = UIColor(hex: "#00C26F")
    static let indicativeSuccess100: UIColor  = UIColor(hex: "#008F52")
    static let indicativeSuccess110: UIColor  = UIColor(hex: "#005C35")
    
    static let indicativeWarning90: UIColor  = UIColor(hex: "#FFC633")
    static let indicativeWarning100: UIColor  = UIColor(hex: "#FFB800")
    static let indicativeWarning110: UIColor  = UIColor(hex: "#CC9300")
    
    static let indicativeError90: UIColor  = UIColor(hex: "#E05257")
    static let indicativeError100: UIColor  = UIColor(hex: "#D8272D")
    static let indicativeError110: UIColor  = UIColor(hex: "#AD1F24")
    
    static let indicativeInformation90: UIColor  = UIColor(hex: "#6B97FF")
    static let indicativeInformation100: UIColor  = UIColor(hex: "#3772FF")
    static let indicativeInformation110: UIColor  = UIColor(hex: "#054FFF")
    
    static let bgGray100: UIColor  = UIColor(hex: "#F9F9F9")
    
    
    // This is for convert color code specific color.
    convenience init(hex: String, alpha: CGFloat = 1) {
        let chars = Array(hex.dropFirst())
        self.init(red:   .init(strtoul(String(chars[0...1]),nil,16))/255,
                  green: .init(strtoul(String(chars[2...3]),nil,16))/255,
                  blue:  .init(strtoul(String(chars[4...5]),nil,16))/255,
                  alpha: alpha)
    }

}

