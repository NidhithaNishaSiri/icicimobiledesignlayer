//
//  IMobileFontStyles.swift
//  MobileSDK
//
//  Created by Tcs on 10/10/22.
//

import Foundation
//
//  UIFont+iMobile.swift
//  Eduvanz
//
//  Created by laxman peramalla on 21/09/22.

import UIKit
public extension UIFont {
    
    func Mulish_Regular_15() -> UIFont {
     return UIFont(name: "Mulish-Regular", size: 15)!
    }
    class func Mulish_Regular_16() -> UIFont {
        
        return UIFont(name: "Mulish-Regular", size: 16)!
    }

    class func Mulish_Regular_17() -> UIFont {
        
        return UIFont(name: "Mulish-Regular", size: 17.0)!
    }

    class func Mulish_Bold_15() -> UIFont {
        
        return UIFont(name: "Mulish-Bold", size: 15)!
    }

    class func Mulish_Bold_17() -> UIFont {
        
        return UIFont(name: "Mulish-Bold", size: 17.0)!
    }

    
}
